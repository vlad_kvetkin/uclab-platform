#encoding "utf8"

TitleName -> 'название';
TitleTitle ->  (QuoteDbl) Word<h-reg1>+ (QuoteDbl); 

Title -> TitleName (AnyWord*) TitleTitle<rt> interp (Title.Title);
Title -> TitleTitle<rt> interp (Title.Title) TitleName (AnyWord*);