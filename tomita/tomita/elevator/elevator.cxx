#encoding "utf8"

ElevatorName -> 'лифт';
ElevatorEnable -> 'есть'|'присутствовать';

ElevatorEnableStr -> ElevatorEnable AnyWord* ElevatorName interp (Elevator.ElevatorEnable);
ElevatorEnableStr -> ElevatorName interp (Elevator.ElevatorEnable) AnyWord*;