#encoding "utf8"

BathroomTypeName -> 'тип';
BathroomName -> 'санузел';
BathroomType -> 'совместный'|'раздельный';
BathroomTypeAt -> 'в'|'во'|'еще'|'ещё'|'под';

BathroomTypeStr -> BathroomType interp (Bathroom.Bathroom) BathroomName;
BathroomTypeStr -> (BathroomName) (BathroomTypeAt) BathroomType interp (Bathroom.Bathroom) (BathroomTypeName) AnyWord*;
BathroomTypeStr -> (BathroomTypeName) (BathroomName) (Colon) (BathroomTypeAt) BathroomType interp (Bathroom.Bathroom);
BathroomTypeStr -> (BathroomName) (BathroomTypeAt) (BathroomTypeName) BathroomType interp (Bathroom.Bathroom);
BathroomTypeStr -> (BathroomTypeAt) BathroomType interp (Bathroom.Bathroom) AnyWord*;