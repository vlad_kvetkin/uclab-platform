#encoding "utf8"

RepairW -> 'ремонт'|'отделка';

RepairType -> 'типовой'|'косметический'|'евро'|'европейский'|'капитальный'|'небольшой'|'кап'|'дизайнерский'|'чистовой'|'касметический';

RepairEn -> 'сделать'|'выполнить'|'есть'|'не' 'требовать'|'присутствовать';

RepairDis -> 'требовать'|'рекомендовать'|'отсутствовать'|'нет'|'нету'|'не' 'сделать'|'следовать' 'сделать'|'следовать' 'проводить'|'нужный'|'нуждаться'|'не' 'быть';

Repair -> ('под') RepairType RepairW|'евроремонт'|'капремонт';

RepairStr -> RepairEn interp (Repair.RepairEn::not_norm) AnyWord* Repair interp (Repair.Repair::not_norm);

RepairStr -> RepairDis interp (Repair.RepairEn::not_norm) AnyWord* Repair interp (Repair.Repair::not_norm);

RepairStr -> RepairW interp (Repair.Repair::not_norm) AnyWord* RepairDis interp (Repair.RepairEn::not_norm);