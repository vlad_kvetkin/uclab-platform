#encoding "utf8"

DayOfWeek -> Noun<kwtype="День недели подачи объявления">;     
Day -> AnyWord<wff=/([1-2]?[0-9])|(3[0-1])/>;
Month -> Noun<kwtype="Месяц даты подачи объявления">;   
YearDescr -> "год" | "г. ";
Year -> AnyWord<wff=/[1-2]?[0-9]{1,3}г?\.?/>; 
Year -> Year YearDescr;

AddSubmissionName -> "подача" | "публикация";
ModifySubmissionName -> "обновление" | "изменение";
SubmissionDateName -> "дата" (AddSubmissionName) (SimConjAnd) (ModifySubmissionName) "объявление";

Date -> DayOfWeek interp (SubmissionDate.DayOfWeek) (Comma)
        Day interp (SubmissionDate.Day) 
        Month interp (SubmissionDate.Month)
        (Year interp (SubmissionDate.Year)); 
        
Date -> Day interp (SubmissionDate.Day)
        Month interp (SubmissionDate.Month)
        (Year interp (SubmissionDate.Year));
      
Date -> Month interp (SubmissionDate.Month)
        Year interp (SubmissionDate.Year);

Date -> Word<kwtype="Срок аренды">;

SubmissionDate -> (QuoteDbl) (SubmissionDateName) (QuoteDbl) (Colon) Date interp (SubmissionDate.Date::not_norm);
SubmissionDate -> Word<kwtype="Срок аренды">;