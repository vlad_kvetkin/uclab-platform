#encoding "utf8"

InternetName -> 'интернет';
InternetEnable -> 'есть'|'присутствовать'|'настроено'|'проведен';

InternetEnableStr -> InternetEnable AnyWord* InternetName interp (Internet.Internet);
InternetEnableStr -> InternetName interp (Internet.Internet) AnyWord*;