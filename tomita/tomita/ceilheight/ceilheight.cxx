#encoding "utf8"

CeilHeightName -> 'высота';
CeilName -> 'потолок';
Meters -> 'м'|'м.'|'метр'<gram="род">;

Square -> CeilHeightName CeilName AnyWord<wff=/[1-9][0-9]+/> interp (CeilingHeight.Height) Meters;
Square -> CeilName CeilHeightName AnyWord<wff=/[1-9][0-9]+/> interp (CeilingHeight.Height) Meters;
Square -> CeilHeightName CeilName AnyWord<wff=/[1-9][0-9]+/> interp (CeilingHeight.Height);
Square -> CeilName CeilHeightName AnyWord<wff=/[1-9][0-9]+/> interp (CeilingHeight.Height);