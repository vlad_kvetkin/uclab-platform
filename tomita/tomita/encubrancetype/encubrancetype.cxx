#encoding "utf8"

EncubranceTypeName -> 'тип';
EncubranceName -> 'обременение';
EncubranceType -> 'аренда'|'ипотека';
EncubranceTypeAt -> 'в'|'во'|'еще'|'ещё'|'под';

EncubrancTypeStr -> EncubranceType interp (EncubranceType.EncubranceType) EncubranceName;
EncubrancTypeStr -> (EncubranceName) (EncubranceTypeAt) EncubranceType interp (EncubranceType.EncubranceType) (EncubranceTypeName) AnyWord*;
EncubrancTypeStr -> (EncubranceTypeName) (EncubranceName) (Colon) (EncubranceTypeAt) EncubranceType interp (EncubranceType.EncubranceType);
EncubrancTypeStr -> (EncubranceName) (EncubranceTypeAt) (EncubranceTypeName) EncubranceType interp (EncubranceType.EncubranceType);
EncubrancTypeStr -> (EncubranceTypeAt) EncubranceType interp (EncubranceType.EncubranceType) AnyWord*;