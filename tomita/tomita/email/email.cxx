#encoding "utf8"

EmailName -> 'email'|'e' Hyphen 'mail'|'почта'|'электронный' 'почта';
EmailPattern -> (EmailName) AnyWord<wff=/[A-Za-z0-9][A-Za-z0-9\.\-_]*[A-Za-z0-9]*@([A-Za-z0-9]+([A-Za-z0-9\-]*[A-Za-z0-9]+)*\.)+[A-Za-z]*/> interp (Email.Email);