#encoding "utf8"

CraneName -> 'кран';
CraneType -> 'мостовой'|'кран-бал'|'кран' Hyphen 'бал'|'козловой';

CraneTypeStr -> CraneName (AnyWord*) CraneType interp (Crane.Crane);
CraneTypeStr -> (CraneName) (AnyWord*) CraneType interp (Crane.Crane);
CraneTypeStr -> CraneType interp (Crane.Crane) (AnyWord*) CraneName;