#encoding "utf8"

HeatingTypeName -> 'тип';
HeatingName -> 'отопление';
HeatingType -> 'газовый'|'угольный'|'автономный'|'центральный'|'централизованный';
HeatingTypeAt -> 'в'|'во'|'еще'|'ещё'|'под';

HeatingTypeStr -> HeatingType interp (Heating.Type) HeatingName;
HeatingTypeStr -> (HeatingName) (HeatingTypeAt) HeatingType interp (Heating.Type) (HeatingTypeName) AnyWord*;
HeatingTypeStr -> (HeatingTypeName) (HeatingName) (Colon) (HeatingTypeAt) HeatingType interp (Heating.Type);
HeatingTypeStr -> (HeatingName) (HeatingTypeAt) (HeatingTypeName) HeatingType interp (Heating.Type);
HeatingTypeStr -> (HeatingTypeAt) HeatingType interp (Heating.Type) AnyWord*;