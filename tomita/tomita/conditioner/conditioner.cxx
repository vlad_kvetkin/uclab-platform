#encoding "utf8"

ConditionerName -> 'кондиционер';
ConditionerEnable -> 'есть'|'присутствовать'|'настроено';

ConditionerEnableStr -> ConditionerEnable AnyWord* ConditionerName interp (Conditioner.Conditioner);
ConditionerEnableStr -> ConditionerName interp (Conditioner.Conditioner) AnyWord*;