#encoding "utf8"

SquareLive -> 'жилой';
SquareKitchen -> 'кухня';
SquareAll -> 'общий';
SquareGarage -> 'гараж';
SquareLand -> 'земельный';
SquareRoom -> 'комната'|'комнат';
SquareDescr -> 'площадь';
SquareName -> 'участок'|'квартира';
Kvadr -> 'кв'|'кв.'|'квадратные'<gram="род">|'квадр';
Meters -> 'м'|'м.'|'метр'<gram="род">;

SquareW -> Kvadr Meters| Meters Kvadr|'м2';

Square -> (SquareDescr <cut>) SquareKitchen<cut> AnyWord<wff=/[1-9][0-9]+/> interp (Square.SquareKitchen) SquareW;
Square -> (SquareDescr <cut>) SquareKitchen<cut> AnyWord<wff=/[1-9][0-9]+/> interp (Square.SquareKitchen);

Square -> (SquareDescr <cut>) SquareGarage<cut> AnyWord<wff=/[1-9][0-9]+/> interp (Square.SquareGarage) SquareW;
Square -> (SquareDescr <cut>) SquareGarage<cut> AnyWord<wff=/[1-9][0-9]+/> interp (Square.SquareGarage);

Square -> SquareLive (SquareDescr <cut>) (SquareName <cut>) AnyWord<wff=/[1-9][0-9]+/> interp (Square.SquareLive) SquareW;
Square -> SquareLive (SquareDescr <cut>) (SquareName <cut>) AnyWord<wff=/[1-9][0-9]+/> interp (Square.SquareLive);

Square -> SquareLand (SquareDescr <cut>) (SquareName <cut>) AnyWord<wff=/[1-9][0-9]+/> interp (Square.SquareLand) SquareW;
Square -> SquareLand (SquareDescr <cut>) (SquareName <cut>) AnyWord<wff=/[1-9][0-9]+/> interp (Square.SquareLand);
Square -> SquareLand (SquareName <cut>) (SquareDescr <cut>) AnyWord<wff=/[1-9][0-9]+/> interp (Square.SquareLand);
Square -> SquareLand (SquareName <cut>) (SquareDescr <cut>) AnyWord<wff=/[1-9][0-9]+/> interp (Square.SquareLand) SquareW;

Square -> (SquareDescr <cut>) SquareRoom <cut> AnyWord<wff=/[1-9][0-9]+/> interp (Square.SquareRoom) SquareW;
Square -> (SquareDescr <cut>) SquareRoom <cut> AnyWord<wff=/[1-9][0-9]+/> interp (Square.SquareRoom);

Square -> SquareAll (SquareName <cut>) (SquareDescr <cut>) AnyWord<wff=/[1-9][0-9]+/> interp (Square.Square) SquareW;
Square -> SquareAll (SquareName <cut>) (SquareDescr <cut>) AnyWord<wff=/[1-9][0-9]+/> interp (Square.Square);
Square -> (SquareDescr <cut>) (SquareName <cut>) AnyWord<wff=/[1-9][0-9]+/> interp (Square.Square) SquareW;
Square -> (SquareDescr <cut>) (SquareName <cut>) AnyWord<wff=/[1-9][0-9]+м2/> interp (Square.Square);