#encoding "utf8"

GarageName -> 'гараж';
GarageTypeName -> 'встроенный'|'машиноместо'|'капитальный'|'встроенный'|'бокс'|'гараж';

GarageType -> GarageName (AnyWord*) GarageTypeName interp (GarageType.Type);
GarageType -> (GarageName) (AnyWord*) GarageTypeName interp (GarageType.Type);
GarageType -> GarageTypeName interp (GarageType.Type) (AnyWord*) GarageName;