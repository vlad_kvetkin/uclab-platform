#encoding "utf8"

ConstructionName -> 'постройка'|'построить'|'построен';
YearDescr -> "год" | "г. ";
Year -> AnyWord<wff=/[1-2]?[0-9]{1,3}г?\.?/>; 
Year -> Year YearDescr;

Construction -> YearDescr ConstructionName (AnyWord*)  Year interp (YearOfConstruction.YearOfConstruction);
Construction -> (AnyWord*) Year interp (YearOfConstruction.YearOfConstruction) ConstructionName YearDescr;
Construction -> ConstructionName (AnyWord*)  Year interp (YearOfConstruction.YearOfConstruction) YearDescr;
Construction -> ConstructionName YearDescr (AnyWord*) Year interp (YearOfConstruction.YearOfConstruction);