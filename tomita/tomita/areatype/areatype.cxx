#encoding "utf8"

AreaTypeName -> 'тип';
AreaName -> 'участок';
AreaType -> 'ферма'|'фермерский' 'хозяйство'|'садоводство';

AreaTypeStr ->  (AreaTypeName) AreaName (AnyWord*) AreaType interp (AreaType.AreaType);
AreaTypeStr -> (AreaTypeName) (AreaName) (AnyWord*) AreaType interp (AreaType.AreaType);
AreaTypeStr -> AreaType interp (AreaType.AreaType) (AnyWord*) (AreaTypeName) AreaName;