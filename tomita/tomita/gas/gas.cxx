#encoding "utf8"

GasName -> 'газ';
GasEnable -> 'есть'|'присутствовать'|'иметь';

GasEnableStr -> GasEnable AnyWord* GasName interp (Gas.GasEnable);
GasEnableStr -> GasName interp (Gas.GasEnable) AnyWord*;