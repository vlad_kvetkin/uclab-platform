#encoding "utf8"

AppDelim -> "," ("ни") | "и";

App -> (AppDelim) 'холодильник'|'холодос';
App -> (AppDelim) 'сплит'|'сплит-система';
App -> (AppDelim) 'кондиционер'|'кандиционер'|'кондер'|'кандер'|'кандёр'|'кондёр';
App -> (AppDelim) 'мойка'|'посудомойка';
App -> (AppDelim) 'стиральная' 'машина'|'стиральная' 'машинка'|'стиралка';
App -> (AppDelim) 'телевизор'|'телек'|'tv';
App -> (AppDelim) 'музцентр'|'музыкальный' 'центр'|'муз' 'центр';
App -> (AppDelim) 'интернет';
App -> (AppDelim) 'плита';
App -> (AppDelim) 'вытяжка';
App -> (AppDelim) 'духовка';
App -> (AppDelim) 'wi-fi';
App -> (AppDelim) 'wifi';


AppC -> (AnyWord*) App+ (AnyWord*) Punct;

Apps -> AppC interp (Apps.App::not_norm);