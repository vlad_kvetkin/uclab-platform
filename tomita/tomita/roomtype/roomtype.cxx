#encoding "utf8"

RoomType -> 'тип';
RoomName -> 'помещение';
RoomTypeName -> 'street' 'retail'|'торговый' 'комплекс';
RoomTypeAt -> 'в'|'во'|'еще'|'ещё';

RoomTypeStr -> RoomTypeName interp (RoomType.Type) RoomName;
RoomTypeStr -> (RoomName) (RoomTypeAt) RoomTypeName interp (RoomType.Type) (RoomType) AnyWord*;
RoomTypeStr -> (RoomType) (RoomName) (Colon) (RoomTypeAt) RoomTypeName interp (RoomType.Type);
RoomTypeStr -> (RoomName) (RoomTypeAt) (RoomType) RoomTypeName interp (RoomType.Type);
RoomTypeStr -> (RoomTypeAt) RoomTypeName interp (RoomType.Type) AnyWord*;