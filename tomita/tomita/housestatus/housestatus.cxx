#encoding "utf8"

HouseStatusName -> 'состояние';
HouseName -> 'дом'|'квартира'|'комната'|'жилье'|'здание';
HouseLegalStatus -> 'строится'|'строящееся'|'вторичка'|'новостройка'|'вторичный'|'вторичке'|'строить'|'строящийся'|'бизнес-центр'|'бизнес' Hyphen 'центр';
HouseLegalStatusAt -> 'в'|'во'|'еще'|'ещё';

HouseLegalStatusStr -> HouseName HouseLegalStatusAt AnyWord<wff=/строящемся|строится|строящееся/> interp (StructurAndLegalStatus.Status) (HouseName);
HouseLegalStatusStr -> HouseLegalStatus interp (StructurAndLegalStatus.Status) HouseName;
HouseLegalStatusStr -> (HouseName) (HouseLegalStatusAt) HouseLegalStatus interp (StructurAndLegalStatus.Status) (HouseStatusName) AnyWord*;
HouseLegalStatusStr -> (HouseStatusName) (HouseName) (Colon) (HouseLegalStatusAt) HouseLegalStatus interp (StructurAndLegalStatus.Status);
HouseLegalStatusStr -> (HouseName) (HouseLegalStatusAt) (HouseStatusName) HouseLegalStatus interp (StructurAndLegalStatus.Status);
HouseLegalStatusStr -> (HouseLegalStatusAt) HouseLegalStatus interp (StructurAndLegalStatus.Status) AnyWord*;
HouseLegalStatusStr -> (HouseLegalStatusAt) AnyWord<wff=/строящемся|строящееся|Строящееся/> interp (StructurAndLegalStatus.Status);