#encoding "utf8"

LayoutName -> 'планировка';
LayoutType -> 'открытый'|'кабинетный';

LayoutTypeStr -> LayoutName (AnyWord*) LayoutType interp (Layout.LayoutType);
LayoutTypeStr -> (LayoutName) (AnyWord*) LayoutType interp (Layout.LayoutType);
LayoutTypeStr -> LayoutType interp (Layout.LayoutType) (AnyWord*) LayoutName;