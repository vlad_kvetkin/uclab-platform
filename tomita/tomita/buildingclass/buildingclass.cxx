#encoding "utf8"

BuildingClassName -> 'класс';
BuildingName -> 'жилье'|'дом'|'квартира'|'комната'|'здание'|'участок'|'гараж';
BuildingClassType -> 'a'|'a+'|'b'|'b+'|'a' PlusSign|'a' PlusSign;

BuildingClassStr -> BuildingName BuildingClassType interp (BuildingClass.BuildingClass) (BuildingClassName);
BuildingClassStr -> (BuildingClassName) (BuildingName) BuildingClassType interp (BuildingClass.BuildingClass);
BuildingClassStr -> BuildingClassType interp (BuildingClass.BuildingClass) (BuildingClassName) (BuildingName);