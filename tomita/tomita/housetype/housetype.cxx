#encoding "utf8"

HouseTypeName -> 'тип';
HouseName -> 'дом'|'квартира'|'комната'|'жилье';
HouseTypeStatus -> 'кирпичный'|'деревянный'|'монолитный'|'панельный';
HouseTypeAt -> 'в'|'во'|'еще'|'ещё'|'под';

HouseTypeStr -> HouseTypeStatus interp (HouseType.Type) HouseName;
HouseTypeStr -> (HouseName) (HouseTypeAt) HouseTypeStatus interp (HouseType.Type) (HouseTypeName) AnyWord*;
HouseTypeStr -> (HouseTypeName) (HouseName) (Colon) (HouseTypeAt) HouseTypeStatus interp (HouseType.Type);
HouseTypeStr -> (HouseName) (HouseTypeAt) (HouseTypeName) HouseTypeStatus interp (HouseType.Type);
HouseTypeStr -> (HouseTypeAt) HouseTypeStatus interp (HouseType.Type) AnyWord*;