#encoding "utf8"

GateName -> 'ворота';
GateTypeName -> 'на' 'пандус'|'доковый' 'тип'|'доковый'|'на' 'нулевой' 'отметка';

GateType -> GateName (AnyWord*) GateTypeName interp (Gate.Gate);
GateType -> (GateName) (AnyWord*) GateTypeName interp (Gate.Gate);
GateType -> GateTypeName interp (Gate.Gate) (AnyWord*) GateName;