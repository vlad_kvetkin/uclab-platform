#encoding "utf8"

SpecName -> 'специализация';
HouseName -> 'здание'|'помещение'|'торговый' 'площадь'|'площадь'|'помещение' 'свободный' 'назначение';
Specialization -> 'кафе'|'кофейня'|'минимаркет'|'офис'|'офис' 'продаж'|'другое';
SpecializationAt -> 'в'|'во';

SpecializationStr -> Specialization interp (Specialization.Specialization) HouseName;
SpecializationStr -> (HouseName) (SpecializationAt) Specialization interp (Specialization.Specialization) (SpecName) AnyWord*;
SpecializationStr -> (SpecName) (HouseName) (Colon) (SpecializationAt) Specialization interp (Specialization.Specialization);
SpecializationStr -> (HouseName) (SpecializationAt) (SpecName) Specialization interp (Specialization.Specialization);
SpecializationStr -> (SpecializationAt) Specialization interp (Specialization.Specialization) AnyWord*;