#encoding "utf8"

HouseClassName -> 'класс';
HouseType -> 'жилье'|'дом'|'квартира'|'комната';
HouseClassType -> 'эконом'|'бизнес'|'элитный';

HouseClassStr -> HouseType HouseClassType interp (HousingClass.Class) (HouseClassName);
HouseClassStr -> (HouseClassName) (HouseType) HouseClassType interp (HousingClass.Class);
HouseClassStr -> HouseClassType interp (HousingClass.Class) (HouseClassName) (HouseType);