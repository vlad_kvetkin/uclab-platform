#encoding "utf8"

ColumnName -> 'колонна';
GridName -> 'сетка';
Multi -> 'x'|'х'|'на';
ColumnGrid -> AnyWord<wff=/[0-9]+/> (Multi) AnyWord<wff=/[0-9]+/>;
ColumnGridPattern -> AnyWord* GridName (ColumnName) ColumnGrid interp (ColumnGrid.Grid) AnyWord*;
ColumnGridPattern -> AnyWord* (ColumnName) GridName ColumnGrid interp (ColumnGrid.Grid) AnyWord*;