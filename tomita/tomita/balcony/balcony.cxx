#encoding "utf8"

BalconyName -> 'балкон'|'лоджия';
BalconyEnable -> 'есть'|'присутствовать';

BalconyEnableStr -> BalconyEnable AnyWord* BalconyName interp (Balcony.Balcony);
BalconyEnableStr -> BalconyName interp (Balcony.Balcony) AnyWord*;