#encoding "utf8"

EntryName -> 'вход';
EntryTypeName -> 'свободный'|'по' 'пропуск'|'общий' 'с' 'улица'|'общий'|'отдельный'|'общий' 'со' 'двор'|'отдельный' 'со' 'двор'|'отдельный' 'с' 'улица';

EntryType -> EntryName (AnyWord*) EntryTypeName interp (Entry.Type);
EntryType -> (EntryName) (AnyWord*) EntryTypeName interp (Entry.Type);
EntryType -> EntryTypeName interp (Entry.Type) (AnyWord*) EntryName;