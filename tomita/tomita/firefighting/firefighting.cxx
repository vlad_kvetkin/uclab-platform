#encoding "utf8"

FireTypeName -> 'система';
FireName -> 'пожаротушение';
FireType -> 'гидрантная'|'сигнализация'|'гидрантная';

FireTypeStr ->  (FireTypeName) FireName (AnyWord*) FireType interp (Firefighting.Firefighting);
FireTypeStr -> (FireTypeName) (FireName) (AnyWord*) FireType interp (Firefighting.Firefighting);
FireTypeStr -> FireType interp (Firefighting.Firefighting) (AnyWord*) (FireTypeName) FireName;