#encoding "utf8"

SecurityName -> 'охрана';
SecurityEnable -> 'есть'|'присутствовать'|'настроено'|'проведен';

SecurityEnableStr -> SecurityEnable AnyWord* SecurityName interp (Security.Security);
SecurityEnableStr -> SecurityName interp (Security.Security) AnyWord*;