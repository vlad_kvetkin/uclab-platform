#encoding "utf8"

TypeName -> 'тип';
ParkingName -> 'парковка';
ParkingType -> 'подземный'|'наземный'|'многоуровневый'|'на' 'крыша';
ParkingAt -> 'в'|'во';

ParkingTypeStr -> ParkingType interp (ParkingType.Type) ParkingName;
ParkingTypeStr -> (ParkingName) (ParkingAt) ParkingType interp (ParkingType.Type) (TypeName) AnyWord*;
ParkingTypeStr -> (TypeName) (ParkingName) (Colon) (ParkingAt) ParkingType interp (ParkingType.Type);
ParkingTypeStr -> (ParkingName) (ParkingAt) (TypeName) ParkingType interp (ParkingType.Type);
ParkingTypeStr -> (ParkingAt) ParkingType interp (ParkingType.Type) AnyWord*;