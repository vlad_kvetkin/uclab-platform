#encoding "utf8"
#GRAMMAR_ROOT ROOT

NumberFull -> AnyWord<wff=/\d{4,}([а-яА-Я])*/> interp (Price.Full);
NumberShort -> AnyWord<wff=/\d{1,3}([а-яА-Я])*/> interp (Price.Short);
NumberHalf -> AnyWord<wff=/\d{1,2}([^а-яА-Я\d])\d{1,3}([а-яА-Я])*/> interp (Price.Half);
Number -> NumberFull | NumberShort | NumberHalf;

P1 -> AnyWord<wff=/вместе/> AnyWord<wff=/с/>;
P2 -> AnyWord<wff=/(\+|и)/>;
Plus -> P1 | P2;

Price -> 'цена' | 'оплата' | 'стоимость' | 'ценник' | 'аренда';

AI1 -> AnyWord<wff=/вс(ё|е)/> AnyWord<wff=/включе(н|нн)о/>;
AI2 -> AnyWord<wff=/(за|и)/> AnyWord<wff=/вс(ё|е)/>;

AllInclude -> AI1 | AI2;

C1 -> Word<wff=/ку/>;
C2 -> Word<wff=/к/> AnyWord<wff=/(\.|\/)/> Word<wff=/у/>;
C3 -> Word<wff=/свет/>;
C4 -> Word<wff=/коммуналка/>;
C5 -> Word<wff=/ком/>;
C6 -> Word<wff=/(электро.*|вод.*|свет)/>;

Communal -> C1 | C2 | C3 | C4 | C5 | C6;

PL1 -> Word<wff=/без/> Word<wff=/залога/>;
PL2 -> Plus Word<wff=/залог/>;
Deposit -> PL1;

Square -> AnyWord<wff=/м2|м^2/>;
InSquare -> AnyWord<wff=/(\/|за)/> AnyWord<wff=/м2.*/>;
InMonth -> AnyWord<wff=/(\/|в)/> AnyWord<wff=/мес.*/>;

NN -> AnyWord<wff=/\D*/>;
NotNumber -> NN | NN NN;

CU1 -> AnyWord<wff=/(р|Р|рублей|руб)/>;
CU2 -> AnyWord<wff=/(млн|к|т|тыс)(\.|\/)?(р|рублей|руб)?\.?/>;
CU3 -> AnyWord<wff=/(р|рублей|руб)\.?/>;
CU4 -> AnyWord<wff=/(млн|к|т|тыс)/> AnyWord<wff=/(р\.?|рублей|руб\.?)/>;
CU5 -> AnyWord<wff=/(тр)/>;
CU6 -> AnyWord<wff=/(млн)\.?/>;

Currency -> CU1 | CU2 | CU3 | CU4 | CU5 | CU6;

ROOT -> Word<kwtype="Срок аренды">;
ROOT -> Price Number interp (Price.Price) {weight=1};
ROOT -> Price InSquare NotNumber* Number+ interp (Price.inSquare::not_norm) Currency NotNumber* Square;
ROOT -> Price NotNumber* Number+ interp (Price.Price) {weight=1};
ROOT -> Price Number+ interp (Price.Price) {weight=1};
ROOT -> Number+ interp (Price.Price) Plus Communal {weight=1};
ROOT -> Number+ interp (Price.Price) AllInclude {weight=0.5};
ROOT -> Number+ interp (Price.inMonth) InMonth {weight=0.5};
ROOT -> Number+ interp (Price.Price) Deposit {weight=1};
ROOT -> Number+ interp (Price.Communal) Currency Communal {weight=0.5};

ROOT -> Price NotNumber Number+ interp (Price.Price) Currency {weight=1};
ROOT -> Number+ interp (Price.Price) Currency AllInclude {weight=0.5};
ROOT -> Number+ interp (Price.inMonth) Currency InMonth {weight=0.5};
ROOT -> Communal NotNumber Number+ interp (Price.Communal) NotNumber* Number+ interp (Price.Communal) (Currency) InMonth {weight=0.5};
ROOT -> Number+ interp (Price.Price) Currency Deposit {weight=1};
ROOT -> Number+ interp (Price.Price) Currency {weight=0.1};