#encoding "utf8"

HouseLineName -> 'линия' 'дом';
HouseLineType -> 'первый'|'второй';

ParkingTypeStr -> HouseLineName AnyWord* HouseLineType interp (LineOfHouse.LineOfHouse) AnyWord*;
ParkingTypeStr -> AnyWord* HouseLineType interp (LineOfHouse.LineOfHouse) HouseLineName AnyWord*;