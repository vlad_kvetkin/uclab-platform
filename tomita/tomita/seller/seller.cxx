#encoding "utf8"

SellerName -> 'продавец'|'собственник'|'арендодатель';
SellerPerson ->  Word<h-reg1>+; 

Seller -> SellerName (AnyWord*) SellerPerson<rt> interp (Seller.Name);
Seller -> SellerPerson<rt> interp (Seller.Name) SellerName (AnyWord*);