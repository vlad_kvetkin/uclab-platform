#encoding "utf8"

UrlName -> 'url'|'ссылка'|'URL';
http -> 'http'|'https';
HttpPattern -> http Colon '/' '/' AnyWord<wff=/([\w\.\-]+)\.([a-z]{2,6}\.?)(\/[\w\.\-]*)*\/?/>;
WithoutHttp -> 'www'|'www.' Punct AnyWord<wff=/([\w\.\-]+)\.([a-z]{2,6}\.?)(\/[\w\.\-]*)*\/?/>;
WithoutAll -> AnyWord<wff=/([\w\.\-]+)\.([a-z]{2,6}\.?)(\/[\w\.\-]*)*\/?/>;
UrlPattern -> HttpPattern|WithoutHttp|WithoutAll;
Url -> (UrlName) (AnyWord*) UrlPattern interp (AdvertismentURL.URL);
Url -> UrlName AnyWord* UrlPattern interp (AdvertismentURL.URL);