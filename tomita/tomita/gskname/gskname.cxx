#encoding "utf8"

GskName -> 'название'|'имя';
Gsk ->  'гск'|'ГСК';
GskText ->  Word<h-reg1>+; 

GskPattern -> (GskName) Gsk (AnyWord*) GskText<rt> interp (NameOfGSK.Name);
GskPattern -> GskText<rt> interp (NameOfGSK.Name) (GskName) Gsk (AnyWord*);