#encoding "utf8"

TelephoneName -> "Телефон" | "Мобильный телефон" | "Моб." | "Домашний телефон";
TelephoneCode -> LBracket AnyWord<wff=/\d+/> RBracket;
MobilePhoneNumberCode -> AnyWord<wff=/8|\+7/>;

FullMobilePhoneNumberWithSpace -> MobilePhoneNumberCode AnyWord<wff=/\d+/>+;
FullMobilePhoneNumberWithDash -> MobilePhoneNumberCode AnyWord<wff=/[\d\-]{7,10}/>;
FullMobilePhoneNumberWithCode -> TelephoneCode AnyWord<wff=/\d+/>+;

MobilePhoneNumber -> FullMobilePhoneNumberWithSpace | FullMobilePhoneNumberWithDash | FullMobilePhoneNumberWithCode;

LandlinePhone -> (TelephoneCode) AnyWord<wff=/\d{2}-?\d{2}-?\d{2}/>;
MobilePhone -> (TelephoneCode) MobilePhoneNumber;

Phones -> MobilePhone | LandlinePhone;
Phone -> (TelephoneName) (Colon) Phones interp (Contacts.Phone);