#encoding "utf8"

Cadastral -> 'кадастровый';
Number -> 'номер';
Pattern1 -> AnyWord<wff=/\d{2}/> Colon AnyWord<wff=/\d{2}/> Colon AnyWord<wff=/\d{1,7}/> Colon AnyWord<wff=/\d+/>;
Pattern2 -> AnyWord<wff=/\d{2}:\d{2}:\d{1,7}:\d{1,}/>;
Pattern -> Pattern1 | Pattern2;
CadastralPattern -> Cadastral (Number) Pattern interp (CadastralNumber.CadastralNumber);