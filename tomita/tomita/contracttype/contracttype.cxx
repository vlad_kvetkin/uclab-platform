#encoding "utf8"

ContractTypeName -> 'тип';
ContractName -> 'договор';
ContractType -> 'продажа'|'перекупка' 'право' 'аренда';

ContractTypeStr ->  (ContractTypeName) ContractName (AnyWord*) ContractType interp (ContractType.ContractType);
ContractTypeStr -> (ContractTypeName) (ContractName) (AnyWord*) ContractType interp (ContractType.ContractType);
ContractTypeStr -> ContractType interp (ContractType.ContractType) (AnyWord*) (ContractTypeName) ContractName;