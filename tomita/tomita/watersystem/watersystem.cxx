#encoding "utf8"

WaterSystemName -> 'водоснабжение';
WaterSystemEnable -> 'есть'|'присутствовать'|'настроено';

WaterSystemStr -> WaterSystemEnable AnyWord* WaterSystemName interp (WaterSystem.WaterSystem);
WaterSystemStr -> WaterSystemName interp (WaterSystem.WaterSystem) AnyWord*;