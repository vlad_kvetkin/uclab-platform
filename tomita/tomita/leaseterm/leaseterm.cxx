#encoding "utf8"

DayOfWeek -> Noun<kwtype="День недели подачи объявления">;     
Day -> AnyWord<wff=/([1-2]?[0-9])|(3[0-1])/>;
Month -> Noun<kwtype="Месяц даты подачи объявления">;   
YearDescr -> "год" | "г. ";
Year -> AnyWord<wff=/[1-2]?[0-9]{1,3}г?\.?/>; 
Year -> Year YearDescr;

LeaseTermName -> "срок";
LeaseName -> "аренда";
LeaseTermDateName -> LeaseTermName (AnyWord*) LeaseName;
LeaseTermDateAt -> 'до';

Date -> DayOfWeek (Comma)
        Day
        Month
        (Year); 
        
Date -> Day
        Month
        (Year);
      
Date -> Month
        Year;

SubmissionDate -> LeaseTermDateName (LeaseTermDateAt) Date interp (LeaseTerm.LeaseTerm);