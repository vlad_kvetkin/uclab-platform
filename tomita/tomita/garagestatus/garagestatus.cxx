#encoding "utf8"

GarageStatusName -> 'статус'|'состояние';
GarageName -> 'гараж'|'квартира'|'комната'|'жилье';
GarageStatus -> 'собственность'|'кооператив'|'по' 'доверенность';
GarageStatusAt -> 'в'|'во';

GarageStatusStr -> GarageStatus interp (GarageStatus.Status) GarageName;
GarageStatusStr -> (GarageName) (GarageStatusAt) GarageStatus interp (GarageStatus.Status) (GarageStatusName) AnyWord*;
GarageStatusStr -> (GarageStatusName) (GarageName) (Colon) (GarageStatusAt) GarageStatus interp (GarageStatus.Status);
GarageStatusStr -> (GarageName) (GarageStatusAt) (GarageStatusName) GarageStatus interp (GarageStatus.Status);
GarageStatusStr -> (GarageStatusAt) GarageStatus interp (GarageStatus.Status) AnyWord*;