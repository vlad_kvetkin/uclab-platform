#encoding "utf8"

FloorName -> 'этаж'|'этажность';
FloorNum -> 'первый'|'второй'|'третий'|'четвертый'|'пятый'|'шестой'|'седьмой'|'восьмой'|'девятый'|'десятый'|'одиннадцатый'|'двенадцатый'|'тренадцатый'|'четырнадцатый'|'пятнадцатый'|'шестнадцатый'|'последний';

FloorNumber -> (AnyWord*) FloorName (AnyWord*) AnyWord<wff=/[1-9]+/> interp (Floor.FloorNumber);
FloorNumber -> AnyWord<wff=/[1-9]+/> interp (Floor.FloorNumber) (AnyWord*) FloorName;

FloorNumber -> (AnyWord*) FloorName FloorNum interp (Floor.FloorNumber);
FloorNumber -> FloorNum interp (Floor.FloorNumber) (AnyWord*) FloorName;