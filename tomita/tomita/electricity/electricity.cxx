#encoding "utf8"

ElectricityName -> 'электричество'|'электрика';
ElectricityEnable -> 'есть'|'присутствовать'|'настроено';

ElectricityEnableStr -> ElectricityEnable AnyWord* ElectricityName interp (Electricity.ElectricityEnable);
ElectricityEnableStr -> ElectricityName interp (Electricity.ElectricityEnable) AnyWord*;