import tomita.parser as tm
import advertisementparser.parser as adv

Tomita = tm.Tomita(r'tomita/tomitaparser.exe', r'../tomita/config.proto')
Tomita.run()
advertisementParser = adv.AdvertisementParser()
result = advertisementParser.xmlParse()
jsonResult = advertisementParser.getJsonFacts(result)
advertisementParser.saveParserResult(jsonResult)