import xml.etree.ElementTree as ET
import json

class AdvertisementParser:
	def xmlParse(self):
		tree = ET.parse('../output/output.xml').getroot()
		result = []
		for document in tree.findall('document/facts'):
			facts = {}
			if document.find('Address') is not None:
				street = document.find('./Address/Street')
				if street is not None:
					facts['street'] = street.attrib.get('val')
				houseNumber = document.find('./Address/House')
				if houseNumber is not None:
					facts['houseNumber'] = houseNumber.attrib.get('val')
				houseBlock = document.find('./Address/Korpus')
				if houseBlock is not None:
					facts['houseBlock'] = houseBlock.attrib.get('val')
			if document.find('Square') is not None:
				square = document.find('./Square/Square')
				if square is not None:
					facts['square'] = square.attrib.get('val')
				squareLive = document.find('./Square/SquareLive')
				if squareLive is not None:
					facts['squareLive'] = squareLive.attrib.get('val')
				squareKitchen = document.find('./Square/SquareKitchen')
				if squareKitchen is not None:
					facts['squareKitchen'] = squareKitchen.attrib.get('val')
				squareRoom = document.find('./Square/SquareRoom')
				if squareRoom is not None:
					facts['squareRoom'] = squareRoom.attrib.get('val')
				squareGarage = document.find('./Square/SquareGarage')
				if squareGarage is not None:
					facts['squareGarage'] = squareGarage.attrib.get('val')
				squareLand = document.find('./Square/SquareLand')
				if squareLand is not None:
					facts['squareLand'] = squareLand.attrib.get('val')
			if document.find('District') is not None:
				district = document.find('./District/District')
				if district is not None:
					facts['district'] = district.attrib.get('val')
			if document.find('Price') is not None:
				price = document.find('./Price/Price')
				if price is not None:
					facts['price'] = price.attrib.get('val')
				fullPrice = document.find('./Price/Full')
				if fullPrice is not None:
					facts['fullPrice'] = fullPrice.attrib.get('val')
				halfPrice = document.find('./Price/Half')
				if halfPrice is not None:
					facts['halfPrice'] = halfPrice.attrib.get('val')
				inMonthPrice = document.find('./Price/inMonth')
				if inMonthPrice is not None:
					facts['inMonthPrice'] = inMonthPrice.attrib.get('val')
				inSquarePrice = document.find('./Price/inSquare')
				if inSquarePrice is not None:
					facts['inSquarePrice'] = inSquarePrice.attrib.get('val')
				communalPrice = document.find('./Price/Communal')
				if communalPrice is not None:
					facts['communalPrice'] = communalPrice.attrib.get('val')
			if document.find('Rooms') is not None:
				rooms = document.find('./Rooms/Rooms')
				if rooms is not None:
					facts['room'] = rooms.attrib.get('val')
			if document.find('Contacts') is not None:
				phone = document.find('./Contacts/Phone')
				if phone is not None:
					facts['contactPhone'] = phone.attrib.get('val')
			if document.find('Rent') is not None:
				rentType = document.find('./Rent/Type')
				if rentType is not None:
					facts['rentType'] = rentType.attrib.get('val')
			if document.find('Apps') is not None:
				app = document.find('./Apps/App')
				if app is not None:
					facts['app'] = app.attrib.get('val')
			if document.find('Repair') is not None:
				repair = document.find('./Repair/Repair')
				if repair is not None:
					facts['repair'] = repair.attrib.get('val')
				repairEn = document.find('./Repair/RepairEn')
				if repairEn is not None:
					facts['repairEn'] = repairEn.attrib.get('val')
			if document.find('SubmissionDate') is not None:
				dayOfWeek = document.find('./SubmissionDate/DayOfWeek')
				if dayOfWeek is not None:
					facts['dayOfWeek'] = dayOfWeek.attrib.get('val')
				day = document.find('./SubmissionDate/Day')
				if day is not None:
					facts['day'] = day.attrib.get('val')
				month = document.find('./SubmissionDate/Month')
				if month is not None:
					facts['month'] = month.attrib.get('val')
				year =  document.find('./SubmissionDate/Year')
				if year is not None:
					facts['year'] = year.attrib.get('val')
				date = document.find('./SubmissionDate/Date')
				if date is not None:
					facts['date'] = date.attrib.get('val')
			if document.find('Description') is not None:
				description = document.find('./Description/Description')
				if description is not None:
					facts['description'] = description.attrib.get('val')
			if document.find('HousingClass') is not None:
				housingClass = document.find('./HousingClass/Class')
				if housingClass is not None:
					facts['housingClass'] = housingClass.attrib.get('val')
			if document.find('StructurAndLegalStatus') is not None:
				legalStatus = document.find('./StructurAndLegalStatus/Status')
				if legalStatus is not None:
					facts['legalStatus'] = legalStatus.attrib.get('val')
			if document.find('CeilingHeight') is not None:
				height = document.find('./CeilingHeight/Height')
				if height is not None:
					facts['ceilingHeight'] = height.attrib.get('val')
			if document.find('GarageType') is not None:
				garageType = document.find('./GarageType/HTypeeight')
				if garageType is not None:
					facts['garageType'] = garageType.attrib.get('val')
					facts['garageExist'] = 'ЕСТЬ'
			if document.find('Seller') is not None:
				name = document.find('./Seller/Name')
				if name is not None:
					facts['sellerName'] = name.attrib.get('val')
			if document.find('Floor') is not None:
				floorNumber = document.find('./Floor/FloorNumber')
				if floorNumber is not None:
					facts['floorNumber'] = floorNumber.attrib.get('val')
			if document.find('GarageStatus') is not None:
				status = document.find('./GarageStatus/Status')
				if status is not None:
					facts['status'] = status.attrib.get('val')
			if document.find('Specialization') is not None:
				specialization = document.find('./Specialization/Specialization')
				if specialization is not None:
					facts['specialization'] = specialization.attrib.get('val')
			if document.find('Email') is not None:
				email = document.find('./Email/Email')
				if email is not None:
					facts['email'] = email.attrib.get('val')
			if document.find('ParkingType') is not None:
				parkingType = document.find('./ParkingType/Type')
				if parkingType is not None:
					facts['parkingType'] = parkingType.attrib.get('val')
					facts['parkingExist'] = 'ЕСТЬ'
			if document.find('YearOfConstruction') is not None:
				yearOfConstruction = document.find('./YearOfConstruction/YearOfConstruction')
				if yearOfConstruction is not None:
					facts['yearOfConstruction'] = yearOfConstruction.attrib.get('val')
			if document.find('HouseType') is not None:
				houseType = document.find('./HouseType/Type')
				if houseType is not None:
					facts['houseType'] = houseType.attrib.get('val')
			if document.find('AdvertismentURL') is not None:
				url = document.find('./AdvertismentURL/URL')
				if url is not None:
					facts['url'] = url.attrib.get('val')
			if document.find('Entry') is not None:
				entry = document.find('./Entry/Type')
				if entry is not None:
					facts['entry'] = entry.attrib.get('val')
			if document.find('RoomType') is not None:
				roomType = document.find('./RoomType/Type')
				if roomType is not None:
					facts['roomType'] = roomType.attrib.get('val')
			if document.find('ColumnGrid') is not None:
				columnGrid = document.find('./ColumnGrid/Grid')
				if columnGrid is not None:
					facts['columnGrid'] = columnGrid.attrib.get('val')
			if document.find('BuildingClass') is not None:
				buildingClass = document.find('./BuildingClass/BuildingClass')
				if buildingClass is not None:
					facts['buildingClass'] = buildingClass.attrib.get('val')
			if document.find('Electricity') is not None:
				electricity = document.find('./Electricity/ElectricityEnable')
				if electricity is not None:
					facts['electricity'] = 'ЕСТЬ'
			if document.find('Elevator') is not None:
				elevator = document.find('./Elevator/ElevatorEnable')
				if elevator is not None:
					facts['elevator'] = 'ЕСТЬ'
			if document.find('Heating') is not None:
				heatingType = document.find('./Heating/Type')
				if heatingType is not None:
					facts['heatingType'] = heatingType.attrib.get('val')
			if document.find('Gas') is not None:
				gas = document.find('./Gas/GasEnable')
				if gas is not None:
					facts['gas'] = 'ЕСТЬ'
			if document.find('Layout') is not None:
				layoutType = document.find('./Layout/LayoutType')
				if layoutType is not None:
					facts['layoutType'] = layoutType.attrib.get('val')
			if document.find('Crane') is not None:
				crane = document.find('./Crane/Crane')
				if crane is not None:
					facts['crane'] = crane.attrib.get('val')
			if document.find('CadastralNumber') is not None:
				cadastralNumber = document.find('./CadastralNumber/CadastralNumber')
				if cadastralNumber is not None:
					facts['cadastralNumber'] = cadastralNumber.attrib.get('val')
			if document.find('Sewerage') is not None:
				sewerage = document.find('./Sewerage/Sewerage')
				if sewerage is not None:
					facts['sewerage'] = sewerage.attrib.get('val')
			if document.find('Gate') is not None:
				gate = document.find('./Gate/Gate')
				if gate is not None:
					facts['gate'] = gate.attrib.get('val')
			if document.find('ContractType') is not None:
				contractType = document.find('./ContractType/ContractType')
				if contractType is not None:
					facts['contractType'] = contractType.attrib.get('val')
			if document.find('Firefighting') is not None:
				firefighting = document.find('./Firefighting/Firefighting')
				if firefighting is not None:
					facts['firefighting'] = firefighting.attrib.get('val')
			if document.find('WaterSystem') is not None:
				waterSystem = document.find('./WaterSystem/WaterSystem')
				if waterSystem is not None:
					facts['waterSystem'] = waterSystem.attrib.get('val')
			if document.find('Furniture') is not None:
				furniture = document.find('./Furniture/Furniture')
				if furniture is not None:
					facts['furniture'] = furniture.attrib.get('val')
			if document.find('Security') is not None:
				security = document.find('./Security/Security')
				if security is not None:
					facts['security'] = security.attrib.get('val')
			if document.find('AreaType') is not None:
				areaType = document.find('./AreaType/AreaType')
				if areaType is not None:
					facts['areaType'] = areaType.attrib.get('val')
			if document.find('Title') is not None:
				title = document.find('./Title/Title')
				if title is not None:
					facts['title'] = title.attrib.get('val')
			if document.find('LeaseTerm') is not None:
				leaseTerm = document.find('./LeaseTerm/LeaseTerm')
				if leaseTerm is not None:
					facts['leaseTerm'] = leaseTerm.attrib.get('val')
			if document.find('Bathroom') is not None:
				bathroom = document.find('./Bathroom/Bathroom')
				if bathroom is not None:
					facts['bathroom'] = bathroom.attrib.get('val')
			if document.find('EncubranceType') is not None:
				encubranceType = document.find('./EncubranceType/EncubranceType')
				if encubranceType is not None:
					facts['encubranceType'] = encubranceType.attrib.get('val')
			if document.find('Conditioner') is not None:
				conditioner = document.find('./Conditioner/Conditioner')
				if conditioner is not None:
					facts['conditioner'] = conditioner.attrib.get('val')
			if document.find('Balcony') is not None:
				balcony = document.find('./Balcony/Balcony')
				if balcony is not None:
					facts['balcony'] = balcony.attrib.get('val')
			if document.find('Internet') is not None:
				internet = document.find('./Internet/Internet')
				if internet is not None:
					facts['internet'] = internet.attrib.get('val')
					
			result.append(facts)

		return result

	def getJsonFacts(self, facts):
		jsonFacts = json.dumps(facts, ensure_ascii=False)
		return jsonFacts

	def saveParserResult(self, parserResult):
		filename = '../output/output.json'
		with open(filename, 'w', encoding='utf-8') as fp:
			fp.write(parserResult)