import subprocess

class Tomita:
	__bin = ''
	__config = ''

	def __init__(self, bin, config):
		self.__bin = bin
		self.__config = config

	def run(self):
		subprocess.run([self.__bin, self.__config])
