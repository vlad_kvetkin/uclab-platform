'use strict';

window.onload = function() {
	highlightFileNames();
	highlightMatches();
}

function highlightFileNames() {
	$('span[title*="txt"]').addClass('file-names-bold');
}

function highlightMatches() {
	$('a[href]').each(function(index) {
		var href = $(this).attr('href').substr(1);
		var matchesText = $(this).text();
		var matchesInText = $('a[name="' + href + '"]');

		var arrMatches = contains(matchesInText, 'span[title]', matchesText);
		for (var j = 0; j < arrMatches.length; j++) {
			arrMatches[j].innerHTML = '<span class="matches-bold">' + arrMatches[j].innerHTML + '</span>';
		}
	});
}

function contains(parent, selector, text) {
  var elements = $(selector, parent);
  text = text.split(' ');

  var matches = [];

  for (var i = 0; i < text.length; i++) {
  	var innerText = parent.text().trim();
  	var haveMatchesInText = RegExp('\\s' + escapeRegExp(text[i]) + '\\s', 'i').test(innerText);

  	if (haveMatchesInText) {
  	    text[i] = text[i].replace(/\)/i, '');
  		var containsSelector = 'span:contains(' + text[i] + ')';
  		$(containsSelector, parent).filter(function() {
  			var regexString = escapeRegExp('^' + text[i] + '$');
  			var haveText =  RegExp(regexString,'i').test($(this).text());
  			if (haveText) {
  				$(this).html('<span class="matches-bold">' + $(this).html() + '</span>');
  			}
  		})
  	} else {
  		Array.prototype.filter.call(elements, function(element) {
  			var title = element.getAttribute('title').trim();

  			var haveMatchesInTitle = RegExp(escapeRegExp(text[i]), 'i').test(title);
  			if (haveMatchesInTitle) {
  				matches.push(element);
  			}
  		});
  	}
  }

  return matches;
}

function escapeRegExp(stringToGoIntoTheRegex) {
    return stringToGoIntoTheRegex.replace(/[-\/\\*+?.()|[\]{}]/g, '\\$&');
}