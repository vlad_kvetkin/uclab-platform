from modules import avito, bepspb, cian, domino, etp_torgi, google, irr, \
    kvadroom, mail, restate, roseltorg, sberbank, sberbank2
from multiprocessing import Queue, Manager, Process
from time import sleep, gmtime, strftime
import json

parsers_for_run = [
    (avito.info, avito.parse),
    (bepspb.info, bepspb.parse),
   # (cian.info, cian.parse),
   # (domino.info, domino.parse),
    #(etp_torgi.info, etp_torgi.parse),
    #(google.info, google.parse),
    (irr.info, irr.parse),
    #(kvadroom.info, kvadroom.parse),
   # (mail.info, mail.parse),
    (restate.info, restate.parse),
   # (roseltorg.info, roseltorg.parse),
   # (sberbank.info, sberbank.parse),
   # (sberbank2.info, sberbank2.parse)
]


def get_curr_time():
    return strftime("%Y-%m-%d-%H-%M-%S", gmtime())


if __name__ == '__main__':
    stop_signal = False
    pipe = Queue()
    manager = Manager()
    parsers = {}
    parserResults = {}
    # запускае процессы для парсеров
    for get_info, parser in parsers_for_run:
        parser_name = get_info()
        parserResults[parser_name] = []
        print(f"> start `{parser_name}' parser @ {get_curr_time()}")
        output_file = open(f'./results/{parser_name}_{get_curr_time()}.json', 'w', encoding='utf-8')
        proc = Process(target=parser, args=(pipe,))
        parsers[parser_name] = (proc, output_file, True)
        proc.start()
    while not stop_signal:
        # обработчик данных
        if not pipe.empty():
            data = pipe.get_nowait()
            if data:
                msg, data = data
                # если парсер подал сигнал об остановке
                if msg.startswith('stop'):
                    curr_parser = msg.split(':')[-1]
                    print(f"> parser `{curr_parser}' stopped @ {get_curr_time()}")
                    proc, file, _ = parsers[curr_parser]
                    parsers[curr_parser] = (proc, file, False)
                    jsonResult = json.dumps(parserResults[curr_parser], ensure_ascii=False)
                    json.dump(jsonResult, file, ensure_ascii=False)
                else:
                    # записываем данные от парсера в файл
                    _, file, _ = parsers[msg]
                    parserResults[msg].append(data)
        # условие останова
        stopped_count = 0
        for _, (_, _, status) in parsers.items():
            if not status:
                stopped_count += 1
        if stopped_count == len(parsers):
            stop_signal = True
    # вырубаем запушенные процессы
    for _, (proc, file, _) in parsers.items():
        proc.join()
