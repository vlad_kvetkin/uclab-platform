from numpy import genfromtxt
import requests
import time
import json

PARSER_NAME = 'google'

types = [
    'accounting', 'airport', 'amusement_park', 'aquarium', 'art_gallery'
    'atm', 'bakery', 'bank', 'bar', 'beauty_salon', 'bicycle_store',
    'book_store', 'bowling_alley', 'bus_station', 'cafe', 'campground',
    'car_dealer', 'car_rental', 'car_repair', 'car_wash', 'casino', 'cemetery',
    'church', 'city_hall', 'clothing_store', 'convenience_store',
    'courthouse', 'dentist', 'department_store', 'doctor', 'electrician',
    'electronics_store', 'embassy', 'fire_station', 'florist', 'funeral_home',
    'furniture_store', 'gas_station', 'gym', 'hair_care', 'hardware_store',
    'hindu_temple', 'home_goods_store', 'hospital', 'insurance_agency',
    'jewelry_store', 'laundry', 'lawyer', 'library', 'liquor_store',
    'local_government_office', 'locksmith', 'lodging', 'meal_delivery',
    'meal_takeaway', 'mosque', 'movie_rental', 'movie_theater',
    'moving_company', 'museum', 'night_club', 'painter', 'park', 'parking',
    'pet_store', 'pharmacy', 'physiotherapist', 'plumber', 'police',
    'post_office', 'real_estate_agency', 'restaurant', 'roofing_contractor',
    'rv_park', 'school', 'shoe_store', 'shopping_mall', 'spa', 'stadium',
    'storage', 'store', 'subway_station', 'synagogue', 'taxi_stand',
    'train_station', 'transit_station', 'travel_agency', 'university',
    'veterinary_care', 'zoo'
]


def info():
    return PARSER_NAME


def parse(queue):
    output_file = genfromtxt('./extra/ru-list.csv', encoding='utf-8', delimiter=';', dtype=None)
    coordinates = []  # список с координатами, где координата - одномерный список \
    # в котором первый элемент - широта, второй - долгота
    for line in output_file:
        coordinate = [line[3], line[4]]  # line[3] - широта, line[4] - долгота из исходного файла
        coordinates.append(coordinate)

    request_count = 0

    def make_request(current_url, current_count):
        if current_count == 1500:
            time.sleep(87000)
            current_count = 0

        request_result = requests.get(current_url)
        current_count = current_count + 1
        return request_result, current_count

    for current_type in types:
        for current_coordinate in coordinates:
            temp_string = str(current_coordinate[0]) + ',' + str(current_coordinate[1])
            # запрашиваем данные об объектах какого-либо типа
            req_url = 'https://maps.googleapis.com/maps/api/place/radarsearch/json?location={}&radius=50000&type={}&key='\
                'AIzaSyAnqpabUobQ6SxojzChNOeTMyEpNPCG0n4'.format(temp_string, current_type)
            r, request_count = make_request(req_url, request_count)
            # получaем json данные об объектах одного типа
            data = r.json()
            # счетчик количества найденных объектов
            count = 0
            # получаем детализированные данные по каждому из объектов, используя place_id
            for item in data['results']:
                temp_id = item['place_id']
                req_url = 'https://maps.googleapis.com/maps/api/place/' \
                          'details/json?placeid=' + temp_id + '&key=AIzaSyDQA3M1z6jCeu8bwYUfA_Skr_bPTtPLV2U'
                r, request_count = make_request(req_url, request_count)
                # увеличиваем счетчик объектов на единицу
                count = count + 1
                # получаем json данные о каждом из объектов
                data = r.json()
                # выводим результат на экран
                # записываем результат в файл
                queue.put_nowait((PARSER_NAME, data))
    queue.put_nowait(('stop:' + PARSER_NAME, None))
