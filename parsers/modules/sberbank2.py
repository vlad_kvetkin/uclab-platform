from bs4 import BeautifulSoup
# import html5lib
import requests
import json

PARSER_NAME = 'sberbank_2'


def get_html1(url):
    return requests.get(url).text


def info():
    return PARSER_NAME


def parse(equeue):
    # Счётчики
    i = 0
    j = 0
    el = 0
    y = 1
    e = 0
    # Cловарь
    dictionary = {
        'Сведения о процедуре': None,
        'Сведения об Организаторе': None,
        'Сведения о заказчике': None,
        'График проведения': None,
        'Сведения о лоте': None
    }
    # Даты и время событий
    events_dates_and_times = []
    # Ссылка лота
    url = 'http://utp.sberbank-ast.ru/VIP/NBT/PurchaseView/43/0/0/275939'
    # Названия событий 
    events_name = []
    # События(Дата и время вместе)
    events = []
    ##########################
    soup = BeautifulSoup(get_html1(url), 'html.parser')
    gh = soup.find('form', id='docForm').find('input', id='xmlData').get('value')
    soup1 = BeautifulSoup(gh, 'html.parser')
    # Сбор ссылки для скачивания документа
    fileID = soup1.find('fileid').text
    link_for_downloadDoc = 'http://utp.sberbank-ast.ru/VIP/File/DownloadFile?fid=' + fileID
    # Сбор дат и времени событий
    for n in soup1.find_all('eventdate'):
        events_dates_and_times.append(soup1.find_all('eventdate')[i].text)
        i += 1
    # Сбор названий событий
    for b in soup1.find_all('eventcomment'):
        events_name.append(soup1.find_all('eventcomment')[j].text)
        j += 1
    # Скленивание дат и названий событий   
    for c in events_name:
        events.append(str(events_dates_and_times[el]) + ' ' + str(events_name[el]))
        el += 1
    ###########################
    dictionary['Сведения о процедуре'] = {
        'Тип процедуры': soup1.find('purchasetypename').text,
        'Вариант проведения процедуры': soup1.find('purchaseaccesslevelname').text,
        'Номер процедуры': soup1.find('purchasecode').text,
        'Наименование процедуры': soup1.find('purchasename').text,
        'Начальная цена процедуры': soup1.find('purchaseamount').text,
        'Регион': soup1.find('regionidname').text,
        'Адрес электронной площадки в сети "Интернет"': soup.find('span', id='PurchaseInfo_Site').text,
        'Статус процедуры': soup1.find('purchasestatus').text
    }

    dictionary['Сведения об Организаторе'] = {
        'Наименование организатора': soup1.find('orgname').text,
        'Полное наименование организатора': soup1.find('orgfullname').text,
        'ИНН организатора': soup1.find('orginn').text,
        'КПП организатора': soup1.find('orgkpp').text,
        'ОГРН организатора': soup1.find('orgogrn').text,
        'Юридический адрес (место нахождения)': soup1.find('orgaddressjur').text,
        'Фактический адрес (почтовый)': soup1.find('orgaddressfact').text,
        'Адрес электронной почты': soup1.find('orgemail').text,
        'Номер контактного телефона': soup1.find('orgphone').text,
        'Контактное лицо': soup1.find('orgcontactperson').text
    }

    dictionary['Сведения о заказчике'] = {
        'Наименование заказчика': soup1.find('customernickname').text,
        'Полное наименование заказчика': soup1.find('customerfullname').text,
        'ИНН заказчика': soup1.find('customerinn').text,
        'КПП заказчика': soup1.find('customerkpp').text,
        'Юридический адрес (место нахождения)': soup1.find('customeraddressjur').text,
        'Фактический адрес (почтовый)': soup1.find('customeraddressfact').text
    }

    dictionary['График проведения'] = {
        'Начало подачи заявок на участие': soup1.find('requeststartdate').text,
        'Окончание подачи заявок на участие': soup1.find('requeststopdate').text,
        'Окончание рассмотрения заявок на участие': soup1.find('requestreviewdate').text,
        'Дата и время начала торгов': soup1.find('auctiondate').text
    }

    dictionary['Сведения о лоте'] = {
        'Номер лота':
        soup1.find('bidno').text,
        'Статус лота':
        soup1.find('bidstatus').text,
        'Наименование лота':
        soup1.find('positionname').text,
        'Начальная цена':
        soup1.find('bidpricenotreq').text,
        'Валюта':
        soup1.find('currencyname').text,
        'Размер обеспечения заявки (задатка) на площадке, руб.':
        soup1.find('bidcoveramount').text,
        'Депозит (комиссия Оператора), руб.':
        soup1.find('biddeposit').text,
        'Информация об обеспечении заявки':
        soup.find('table', id='BidsInfo_BidInfo_3446405').find('tbody').find_all('tr')[7].find(
            'td', class_='block-data').text,
        'Минимальный шаг торгов, в %':
        soup1.find('auctionstepminpercent').text,
        'Максимальный шаг торгов, в %':
        soup1.find('auctionstepmaxpercent').text,
        'Длительность основного времени торгов':
        soup1.find('bidfirstofferperiodidname').text,
        'Время продления торгов':
        soup1.find('bidofferperiodidname').text,
        'Параметры торгов':
        'Отсутствуют',
        'Позиции лота':
        None,
        'Дополнительные регионы':
        'Отсутствуют',
        'Классификатор ОКПД2':
        'Отсутствуют',
        'Документы':
        None
    }

    dictionary['Сведения о лоте']['Позиции лота'] = {
        'Номер': soup1.find('poscode').text,
        'Наименование': soup1.find('positionname').text,
        'Количество': soup1.find('quantity').text,
        'Начальная цена за единицу': soup1.find('bidpricenotreq').text,
        'Единица измерения': soup1.find('currencyname').text,
        'Мин шаг, в валюте': soup1.find('posauctionstepmin').text,
        'Макс шаг, в валюте': soup1.find('posauctionstepmax').text,
        'Мин шаг, в %': soup1.find('auctionstepminpercent').text,
        'Макс шаг, в %': soup1.find('auctionstepmaxpercent').text
    }
    dictionary['Сведения о лоте']['Документы'] = {'Документация': None}
    dictionary['Сведения о лоте']['Документы']['Документация'] = link_for_downloadDoc

    for d in events:
        dictionary['Сведения о лоте']['Событие' + str(y)] = events[e]
        y += 1
        e += 1
    equeue.put_nowait((PARSER_NAME, dictionary))
    equeue.put_nowait(('stop:' + PARSER_NAME, None))
